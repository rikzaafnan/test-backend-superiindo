package handler

import (
	"net/http"
	"strconv"
	"test-backend-superiindo/transaction"
	"test-backend-superiindo/user"
	"test-backend-superiindo/utils"

	"github.com/gin-gonic/gin"
)

type transactionHandler struct {
	transactionService transaction.ServiceTransaction
}

func NewTransactionHandler(transactionService transaction.ServiceTransaction) *transactionHandler {
	return &transactionHandler{transactionService}
}

func (h *transactionHandler) Create(c *gin.Context) {

	var input transaction.InTransaction

	userID := c.Param("userId")
	userIDint, err := strconv.Atoi(userID)
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed create", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	currentUser := c.MustGet("currentUser").(user.User)

	if userIDint != currentUser.ID {
		errorMessage := gin.H{"errors": "accoun not found"}
		response := utils.APIResponse("account not found", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	err = c.ShouldBindJSON(&input)
	if err != nil {

		response := utils.APIResponse("create failed", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newTransaction, err := h.transactionService.Save(c, input)
	if err != nil {
		response := utils.APIResponse("create transaction failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	formatter := transaction.FormatTransaction(newTransaction)

	response := utils.APIResponse("succes create", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *transactionHandler) FindAll(c *gin.Context) {

	var transactionFormatters []transaction.TransactionFormatter

	userID := c.Param("userId")
	userIDint, err := strconv.Atoi(userID)
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed get data", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	currentUser := c.MustGet("currentUser").(user.User)

	if userIDint != currentUser.ID {
		errorMessage := gin.H{"errors": "accoun not found"}
		response := utils.APIResponse("account not found", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	transactions, err := h.transactionService.FindAll()
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("get data failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	for _, transactionValue := range transactions {
		formatter := transaction.FormatTransaction(transactionValue)
		transactionFormatters = append(transactionFormatters, formatter)
	}

	response := utils.APIResponse("Succesfully get data transaction", http.StatusOK, "success", transactionFormatters)

	c.JSON(http.StatusOK, response)

}
