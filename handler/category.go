package handler

import (
	"net/http"
	"test-backend-superiindo/category"
	"test-backend-superiindo/utils"

	"github.com/gin-gonic/gin"
)

type categoryHandler struct {
	categoryService category.Service
}

func NewCategoryHandler(categoryService category.Service) *categoryHandler {
	return &categoryHandler{categoryService}
}

func (h *categoryHandler) Create(c *gin.Context) {

	var input category.CategoryInput

	err := c.ShouldBindJSON(&input)
	if err != nil {

		response := utils.APIResponse("create failed", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newCategory, err := h.categoryService.Create(input)
	if err != nil {
		response := utils.APIResponse("create failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	formatter := category.FormatCategory(newCategory)

	response := utils.APIResponse("succes create", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *categoryHandler) FindAll(c *gin.Context) {

	var categoryFormatters []category.CategoryFormatter

	categories, err := h.categoryService.FindAll()
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed get category", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	for _, categoryValue := range categories {
		formatter := category.FormatCategory(categoryValue)
		categoryFormatters = append(categoryFormatters, formatter)
	}

	response := utils.APIResponse("Succesfully get category", http.StatusOK, "success", categoryFormatters)

	c.JSON(http.StatusOK, response)

}
