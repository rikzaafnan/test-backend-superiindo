package handler

import (
	"net/http"
	"test-backend-superiindo/auth"
	user "test-backend-superiindo/user"
	"test-backend-superiindo/utils"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type userHandler struct {
	service     user.ServiceUser
	serviceAuth auth.Service
}

func NewUserHandler(serviceUser user.ServiceUser, serviceAuth auth.Service) *userHandler {
	return &userHandler{serviceUser, serviceAuth}
}

func (h *userHandler) Register(c *gin.Context) {

	var input user.RegisterUserInput

	err := c.ShouldBindJSON(&input)
	if err != nil {

		response := utils.APIResponse("Register account failed", http.StatusUnprocessableEntity, "error", err)
		log.Error(err)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newUser, err := h.service.Register(input)
	if err != nil {
		response := utils.APIResponse("Register account failed", http.StatusBadRequest, "error", err)
		log.Error(err)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	token, err := h.serviceAuth.GenerateToken(newUser.ID)
	if err != nil {
		response := utils.APIResponse("Register account failed", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	formatter := user.FormatUser(newUser, token)

	response := utils.APIResponse("account has been registered", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *userHandler) Login(c *gin.Context) {

	var input user.LoginInput

	err := c.ShouldBindJSON(&input)
	if err != nil {

		log.Error(err)
		response := utils.APIResponse("login failed", http.StatusUnprocessableEntity, "error", err)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	loggedinUser, err := h.service.Login(input)
	if err != nil {

		log.Error(err)
		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("login failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	token, err := h.serviceAuth.GenerateToken(loggedinUser.ID)
	if err != nil {
		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("login failed", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	// // check validate token
	// validateToken, err := h.serviceAuth.ValidateToken(token)
	// if err != nil {
	// 	errorMessage := gin.H{"errors": err.Error()}
	// 	response := utils.APIResponse("login failed", http.StatusUnprocessableEntity, "error", errorMessage)
	// 	c.JSON(http.StatusBadRequest, response)
	// 	return
	// }

	// log.Info(validateToken.Claims)
	// log.Info(validateToken.Header)
	// log.Info(validateToken.Method)
	// log.Info(validateToken.Valid)
	// log.Info(validateToken.Signature)

	formatter := user.FormatUser(loggedinUser, token)
	response := utils.APIResponse("Succesfully loggedin", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *userHandler) Logout(c *gin.Context) {

	response := utils.APIResponse("Succesfully Logout", http.StatusOK, "success", "")

	c.JSON(http.StatusOK, response)

}
