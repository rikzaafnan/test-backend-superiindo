package handler

import (
	"net/http"
	"strconv"
	"test-backend-superiindo/product"
	"test-backend-superiindo/utils"

	"github.com/gin-gonic/gin"
)

type productHandler struct {
	productService product.ServiceProduct
}

func NewProductHandler(productService product.ServiceProduct) *productHandler {
	return &productHandler{productService}
}

func (h *productHandler) Create(c *gin.Context) {

	var input product.InProduct

	err := c.ShouldBindJSON(&input)
	if err != nil {

		response := utils.APIResponse("create failed", http.StatusUnprocessableEntity, "error", err.Error())
		c.JSON(http.StatusBadRequest, response)
		return
	}

	newProduct, err := h.productService.Save(c, input)
	if err != nil {
		response := utils.APIResponse("failed create data", http.StatusBadRequest, "error", nil)
		c.JSON(http.StatusBadRequest, response)
		return

	}

	formatter := product.FormatProduct(newProduct)

	response := utils.APIResponse("succes create", http.StatusOK, "success", formatter)

	c.JSON(http.StatusOK, response)

}

func (h *productHandler) FindAll(c *gin.Context) {

	var productFormatters []product.ProductFormatter

	queryStringCategory := c.Query("category")

	var queryParam interface{}

	queryParam = queryStringCategory

	products, err := h.productService.FindAll(queryParam)
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed get data", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	for _, productValue := range products {
		formatter := product.FormatProduct(productValue)
		productFormatters = append(productFormatters, formatter)
	}

	response := utils.APIResponse("Succesfully get data", http.StatusOK, "success", productFormatters)

	c.JSON(http.StatusOK, response)

}

func (h *productHandler) FindById(c *gin.Context) {

	var productFormatter product.ProductFormatter

	productID := c.Param("productId")
	productIDint, err := strconv.Atoi(productID)
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed get data", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	productFindById, err := h.productService.FindById(productIDint)
	if err != nil {

		errorMessage := gin.H{"errors": err.Error()}
		response := utils.APIResponse("failed get data", http.StatusUnprocessableEntity, "error", errorMessage)
		c.JSON(http.StatusBadRequest, response)
		return
	}

	productFormatter = product.FormatProduct(productFindById)

	response := utils.APIResponse("Succesfully get data", http.StatusOK, "success", productFormatter)

	c.JSON(http.StatusOK, response)

}
