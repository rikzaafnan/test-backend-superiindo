package transaction

import "gorm.io/gorm"

type RepositoryTransaction interface {
	Save(db *gorm.DB, transaction Transaction) (Transaction, error)
	FindAll(db *gorm.DB) ([]Transaction, error)
}

type repositoryTransaction struct {
}

func NewRepositoryTransaction() *repositoryTransaction {
	return &repositoryTransaction{}
}

func (r *repositoryTransaction) Save(db *gorm.DB, transaction Transaction) (Transaction, error) {

	err := db.Create(&transaction).Error

	if err != nil {
		return transaction, err
	}

	return transaction, nil

}

func (r *repositoryTransaction) FindAll(db *gorm.DB) ([]Transaction, error) {
	var transactions []Transaction

	err := db.Preload("TransactionDetails").Find(&transactions).Error
	if err != nil {

		return transactions, err
	}

	return transactions, nil
}
