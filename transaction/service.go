package transaction

import (
	td "test-backend-superiindo/transactiondetail"
	"test-backend-superiindo/user"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type ServiceTransaction interface {
	Save(c *gin.Context, input InTransaction) (Transaction, error)
	FindAll() ([]Transaction, error)
}

type serviceTransaction struct {
	db                       *gorm.DB
	repositoryTransaction    RepositoryTransaction
	serviceTransactionDetail td.ServiceTransactionDetail
}

func NewServiceTransaction(db *gorm.DB, repositoryTransaction RepositoryTransaction, serviceTransactionDetail td.ServiceTransactionDetail) *serviceTransaction {
	return &serviceTransaction{db, repositoryTransaction, serviceTransactionDetail}
}

func (s *serviceTransaction) Save(c *gin.Context, input InTransaction) (Transaction, error) {
	Transaction := Transaction{}

	currentUser := c.MustGet("currentUser").(user.User)

	Transaction.TotalAmount = input.TotalAmount
	Transaction.CreatedAt = time.Time(time.Now()).UTC()
	Transaction.CreatedBy = currentUser.Name

	tx := s.db.Begin()

	newTransaction, err := s.repositoryTransaction.Save(tx, Transaction)
	if err != nil {
		log.Error(err)
		tx.Rollback()
		return Transaction, err
	}

	for _, transactionDetailValue := range input.TransctionDetails {

		inTransactionDetail := td.InTransactionDetail{}
		inTransactionDetail.Amount = transactionDetailValue.Amount
		inTransactionDetail.ProductID = transactionDetailValue.ProductID
		inTransactionDetail.Qty = transactionDetailValue.Qty

		_, err = s.serviceTransactionDetail.Save(c, inTransactionDetail, newTransaction.ID)
		if err != nil {
			log.Error(err)
			tx.Rollback()
			return Transaction, err
		}
	}

	tx.Commit()

	return newTransaction, nil
}

func (s *serviceTransaction) FindAll() ([]Transaction, error) {

	tx := s.db.Begin()

	Transaction, err := s.repositoryTransaction.FindAll(tx)
	if err != nil {
		log.Error(err)
		tx.Rollback()
		return Transaction, err
	}

	tx.Commit()

	return Transaction, nil

}
