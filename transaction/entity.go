package transaction

import (
	"test-backend-superiindo/transactiondetail"
	"time"
)

type Transaction struct {
	ID                 int `gorm:"primary_key, not null"`
	TotalAmount        float64
	CreatedBy          string
	CreatedAt          time.Time
	UpdatedAt          time.Time
	UpdatedBy          string
	TransactionDetails []transactiondetail.TransactionDetail `gorm:"foreignKey:TransactionID"`
}
