package transaction

type InTransaction struct {
	TotalAmount       float64               `json:"totalAmount" binding:"required"`
	TransctionDetails []InTransactionDetail `json:"transactionDetails" binding:"required"`
}

type InTransactionDetail struct {
	Qty       float64 `json:"qty" binding:"required"`
	ProductID float64 `json:"productId" binding:"required"`
	Amount    float64 `json:"amount" binding:"required"`
}
