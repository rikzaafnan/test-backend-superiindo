package transaction

import (
	"fmt"
	"test-backend-superiindo/transactiondetail"
)

type TransactionFormatter struct {
	ID                 int                                            `json:"id"`
	TotalAmount        float64                                        `json:"totalAmount"`
	TransactionDetails []transactiondetail.TransactionDetailFormatter `json:"transactionDetails"`
}

func FormatTransaction(tx Transaction) TransactionFormatter {

	var transactionDetailFormatters []transactiondetail.TransactionDetailFormatter

	fmt.Println(len(tx.TransactionDetails))
	if len(tx.TransactionDetails) > 0 {

		for _, transactionDetailValue := range tx.TransactionDetails {
			transactionDetailFormat := transactiondetail.FormatTransactionDetail(transactionDetailValue)

			transactionDetailFormatters = append(transactionDetailFormatters, transactionDetailFormat)
		}

	}

	formatter := TransactionFormatter{
		ID:                 tx.ID,
		TotalAmount:        tx.TotalAmount,
		TransactionDetails: transactionDetailFormatters,
	}

	return formatter
}
