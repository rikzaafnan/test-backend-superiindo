package main

import (
	"net/http"
	"strings"
	"test-backend-superiindo/auth"
	"test-backend-superiindo/category"
	configFolder "test-backend-superiindo/config"
	"test-backend-superiindo/handler"
	"test-backend-superiindo/product"
	"test-backend-superiindo/transaction"
	"test-backend-superiindo/transactiondetail"
	"test-backend-superiindo/user"

	"test-backend-superiindo/utils"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

func main() {

	db := configFolder.InitDB()
	// if err != nil {
	// 	log.Error(err)
	// 	log.Fatal(err.Error())
	// }

	authService := auth.NewService()

	userRepository := user.NewRepositoryUser(db)
	userService := user.NewServiceUser(userRepository)
	userHandler := handler.NewUserHandler(userService, authService)

	categoryRepository := category.NewRepository(db)
	categoryService := category.NewService(categoryRepository)
	categoryHandler := handler.NewCategoryHandler(categoryService)

	productRepository := product.NewRepositoryProduct(db)
	productService := product.NewServiceProduct(productRepository, categoryRepository)
	productHandler := handler.NewProductHandler(productService)

	transactionDetailRepository := transactiondetail.NewRepositoryTransactionDetail()
	transactionDetailService := transactiondetail.NewServiceTransactionDetail(db, transactionDetailRepository, productService)

	transactionRepository := transaction.NewRepositoryTransaction()
	transactionService := transaction.NewServiceTransaction(db, transactionRepository, transactionDetailService)
	transactionHandler := handler.NewTransactionHandler(transactionService)

	r := gin.Default()

	v1 := r.Group("/api/v1")
	{
		v1.POST("/register", userHandler.Register)
		v1.POST("/login", userHandler.Login)
		v1.POST("/logout", authMiddleware(authService, userService), userHandler.Logout)

		v1.POST("/categories", authMiddleware(authService, userService), categoryHandler.Create)
		v1.GET("/categories", authMiddleware(authService, userService), categoryHandler.FindAll)

		v1.POST("/products", authMiddleware(authService, userService), productHandler.Create)
		v1.GET("/products", authMiddleware(authService, userService), productHandler.FindAll)
		v1.GET("/products/:productId", authMiddleware(authService, userService), productHandler.FindAll)

		v1.POST("/users/:userId/transactions", authMiddleware(authService, userService), transactionHandler.Create)
		v1.GET("/users/:userId/transactions", authMiddleware(authService, userService), transactionHandler.FindAll)

	}

	r.GET("/ping", func(c *gin.Context) {

		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run(":8181") // listen and serve on 0.0.0.0:8080
}

func authMiddleware(authService auth.Service, userService user.ServiceUser) gin.HandlerFunc {

	return func(c *gin.Context) {

		authHeader := c.GetHeader("Authorization")

		if !strings.Contains(authHeader, "Bearer") {
			response := utils.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		tokenString := ""
		// untuk memisahkan antara bearer dan token
		arraytoken := strings.Split(authHeader, " ")
		if len(arraytoken) == 2 {
			tokenString = arraytoken[1]
		}

		token, err := authService.ValidateToken(tokenString)
		if err != nil {
			response := utils.APIResponse(err.Error(), http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		claim, ok := token.Claims.(jwt.MapClaims)
		if !ok || !token.Valid {

			response := utils.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		userID := int(claim["user_id"].(float64))

		user, err := userService.GetUserByID(userID)
		if err != nil {
			response := utils.APIResponse("Unauthorized", http.StatusUnauthorized, "error", nil)
			c.AbortWithStatusJSON(http.StatusUnauthorized, response)
			return
		}

		// menyimpan data di set untuk bisa dipakai di file manapun
		c.Set("currentUser", user)

	}

}
