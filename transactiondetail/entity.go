package transactiondetail

import "time"

type TransactionDetail struct {
	ID            int `gorm:"primary_key, not null"`
	Amount        float64
	ProductID     int
	Qty           int
	TransactionID int
	CreatedBy     string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	UpdatedBy     string
}
