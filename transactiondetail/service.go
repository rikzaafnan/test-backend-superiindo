package transactiondetail

import (
	"errors"
	"test-backend-superiindo/product"
	"test-backend-superiindo/user"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gorm.io/gorm"
)

type ServiceTransactionDetail interface {
	Save(c *gin.Context, input InTransactionDetail, transactionId int) (TransactionDetail, error)
	FindAll() ([]TransactionDetail, error)
}

type serviceTransactionDetail struct {
	db                          *gorm.DB
	repositoryTransactionDetail RepositoryTransactionDetail
	serviceProduct              product.ServiceProduct
}

func NewServiceTransactionDetail(db *gorm.DB, repositoryTransactionDetail RepositoryTransactionDetail, serviceProduct product.ServiceProduct) *serviceTransactionDetail {
	return &serviceTransactionDetail{db, repositoryTransactionDetail, serviceProduct}
}

func (s *serviceTransactionDetail) Save(c *gin.Context, input InTransactionDetail, transactionId int) (TransactionDetail, error) {
	TransactionDetail := TransactionDetail{}

	currentUser := c.MustGet("currentUser").(user.User)

	TransactionDetail.TransactionID = transactionId
	TransactionDetail.Amount = input.Amount
	TransactionDetail.ProductID = int(input.ProductID)
	TransactionDetail.Qty = int(input.Qty)
	TransactionDetail.CreatedAt = time.Now().UTC()
	TransactionDetail.CreatedBy = currentUser.Name

	tx := s.db.Begin()
	// check product
	_, err := s.serviceProduct.FindById(int(input.ProductID))
	if err != nil {
		ErrProductNotFound := errors.New("product no found")
		err = ErrProductNotFound
		log.Error(err)
		tx.Rollback()
		return TransactionDetail, err
	}

	newTransactionDetail, err := s.repositoryTransactionDetail.Save(tx, TransactionDetail)
	if err != nil {
		log.Error(err)
		tx.Rollback()
		return TransactionDetail, err
	}

	tx.Commit()

	return newTransactionDetail, nil
}

func (s *serviceTransactionDetail) FindAll() ([]TransactionDetail, error) {

	tx := s.db.Begin()

	TransactionDetail, err := s.repositoryTransactionDetail.FindAll(tx)
	if err != nil {
		log.Error(err)
		tx.Rollback()
		return TransactionDetail, err
	}

	tx.Commit()

	return TransactionDetail, nil

}
