package transactiondetail

type TransactionDetailFormatter struct {
	ID        int     `json:"id"`
	Amount    float64 `json:"totalAmount"`
	ProductID int     `json:"productId"`
	Qty       int     `json:"qty"`
}

func FormatTransactionDetail(tx TransactionDetail) TransactionDetailFormatter {
	formatter := TransactionDetailFormatter{
		ID:        tx.ID,
		ProductID: tx.ProductID,
		Amount:    tx.Amount,
		Qty:       tx.Qty,
	}

	return formatter
}
