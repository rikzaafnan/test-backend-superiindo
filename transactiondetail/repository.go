package transactiondetail

import "gorm.io/gorm"

type RepositoryTransactionDetail interface {
	Save(db *gorm.DB, transactionDetail TransactionDetail) (TransactionDetail, error)
	FindAll(db *gorm.DB) ([]TransactionDetail, error)
}

type repositoryTransactionDetail struct {
}

func NewRepositoryTransactionDetail() *repositoryTransactionDetail {
	return &repositoryTransactionDetail{}
}

func (r *repositoryTransactionDetail) Save(db *gorm.DB, transactionDetail TransactionDetail) (TransactionDetail, error) {

	err := db.Create(&transactionDetail).Error

	if err != nil {
		return transactionDetail, err
	}

	return transactionDetail, nil

}

func (r *repositoryTransactionDetail) FindAll(db *gorm.DB) ([]TransactionDetail, error) {
	var transactionDetails []TransactionDetail

	err := db.Find(&transactionDetails).Error
	if err != nil {

		return transactionDetails, err
	}

	return transactionDetails, nil
}
