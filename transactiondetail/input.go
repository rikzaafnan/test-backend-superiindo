package transactiondetail

type InTransactionDetail struct {
	Qty       float64 `json:"qty" binding:"required"`
	ProductID float64 `json:"productId" binding:"required"`
	Amount    float64 `json:"amount" binding:"required"`
}
