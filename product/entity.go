package product

import "time"

type Product struct {
	ID        int `gorm:"primary_key, not null"`
	Name      string
	Category  string
	Pricing   float64
	CreatedBy string
	CreatedAt time.Time
	UpdatedAt time.Time
	UpdatedBy string
}
