package product

import (
	"gorm.io/gorm"
)

type RepositoryProduct interface {
	Save(product Product) (Product, error)
	FindByID(ID int) (Product, error)
	FindAll(metaData interface{}) ([]Product, error)
}

type repositoryProduct struct {
	db *gorm.DB
}

func NewRepositoryProduct(db *gorm.DB) *repositoryProduct {
	return &repositoryProduct{db}
}

func (r *repositoryProduct) Save(product Product) (Product, error) {

	err := r.db.Create(&product).Error

	if err != nil {
		return product, err
	}

	return product, nil

}

func (r *repositoryProduct) FindByID(ID int) (Product, error) {
	var product Product

	err := r.db.Where("id = ?", ID).First(&product).Error
	if err != nil {

		return product, err
	}

	return product, nil
}

func (r *repositoryProduct) FindAll(metaData interface{}) ([]Product, error) {
	var products []Product

	if metaData != nil {
		err := r.db.Where("category = ?", metaData).Find(&products).Error
		if err != nil {

			return products, err
		}
	} else {

		err := r.db.Find(&products).Error
		if err != nil {

			return products, err
		}
	}

	return products, nil
}
