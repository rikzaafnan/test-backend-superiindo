package product

import (
	"test-backend-superiindo/category"
	"test-backend-superiindo/user"
	"time"

	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

type ServiceProduct interface {
	Save(c *gin.Context, input InProduct) (Product, error)
	FindById(id int) (Product, error)
	FindAll(metaData interface{}) ([]Product, error)
}

type serviceProduct struct {
	repositoryProduct  RepositoryProduct
	repositoryCategory category.Repository
}

func NewServiceProduct(repositoryProduct RepositoryProduct, repositoryCategory category.Repository) *serviceProduct {
	return &serviceProduct{repositoryProduct, repositoryCategory}
}

func (s *serviceProduct) Save(c *gin.Context, input InProduct) (Product, error) {
	product := Product{}
	// check category
	category, err := s.repositoryCategory.FindByName(input.Category)
	if err != nil {
		log.Error(err)
		return product, err
	}

	currentUser := c.MustGet("currentUser").(user.User)
	product.Name = input.Name
	product.Category = category.Name
	product.CreatedAt = time.Time(time.Now()).UTC()
	product.Pricing = input.Pricing
	product.CreatedBy = currentUser.Name

	newProduct, err := s.repositoryProduct.Save(product)
	if err != nil {
		log.Error(err)
		return product, err
	}

	return newProduct, nil
}

func (s *serviceProduct) FindById(id int) (Product, error) {
	product, err := s.repositoryProduct.FindByID(id)
	if err != nil {
		log.Error(err)
		return product, err
	}

	return product, nil

}

func (s *serviceProduct) FindAll(metaData interface{}) ([]Product, error) {

	products, err := s.repositoryProduct.FindAll(metaData)
	if err != nil {
		log.Error(err)
		return products, err
	}

	return products, nil

}
