package product

type ProductFormatter struct {
	ID       int     `json:"id"`
	Name     string  `json:"name"`
	Category string  `json:"category"`
	Pricing  float64 `json:"pricing"`
}

func FormatProduct(product Product) ProductFormatter {
	formatter := ProductFormatter{
		ID:       product.ID,
		Name:     product.Name,
		Category: product.Category,
		Pricing:  product.Pricing,
	}

	return formatter
}
