package product

type InProduct struct {
	Name     string  `json:"name" binding:"required"`
	Category string  `json:"category" binding:"required"`
	Pricing  float64 `json:"pricing" binding:"required"`
}
