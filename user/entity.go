package user

import "time"

type User struct {
	ID        int `gorm:"primary_key, not null"`
	Name      string
	Email     string `gorm:"unique;not null"`
	Password  string
	CreatedAt time.Time
	UpdatedAt time.Time
}
