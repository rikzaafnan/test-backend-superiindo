package user

import (
	"errors"

	log "github.com/sirupsen/logrus"

	"golang.org/x/crypto/bcrypt"
)

type ServiceUser interface {
	Register(input RegisterUserInput) (User, error)
	Login(input LoginInput) (User, error)
	GetUserByID(Id int) (User, error)
	Logout() (string, error)
}

type serviceUser struct {
	repositoryUser RepositoryUser
}

func NewServiceUser(repositoryUser RepositoryUser) *serviceUser {
	return &serviceUser{repositoryUser}
}

func (s *serviceUser) Register(input RegisterUserInput) (User, error) {

	user := User{}
	user.Name = input.Name
	user.Email = input.Email
	PasswordHash, err := bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.MinCost)
	if err != nil {
		log.Error(err)
		return user, err
	}
	user.Password = string(PasswordHash)

	// check Email
	userEmail, err := s.repositoryUser.FindByEmail(user.Email)
	if err != nil {
		log.Error(err)
		return User{}, err
	}

	if userEmail.ID != 0 {
		log.Error(err)
		return User{}, errors.New("email telah digunakan")
	}

	newUser, err := s.repositoryUser.Save(user)
	if err != nil {
		log.Error(err)
		return newUser, err
	}

	return newUser, nil
}

func (s *serviceUser) Login(input LoginInput) (User, error) {
	email := input.Email
	password := input.Password

	log.Info(input)

	user, err := s.repositoryUser.FindByEmail(email)
	if err != nil {
		log.Error(err)
		return user, err
	}

	if user.ID == 0 {
		log.Error(err)
		return user, errors.New("no user found on that email")
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err != nil {
		log.Error(err)
		return user, err
	}

	return user, nil

}

func (s *serviceUser) GetUserByID(ID int) (User, error) {

	user, err := s.repositoryUser.FindByID(ID)
	if err != nil {
		log.Error(err)
		return user, err
	}

	if user.ID == 0 {
		log.Error(err)
		return user, errors.New("no user found on with that ID")
	}

	return user, nil

}

func (s *serviceUser) Logout() (string, error) {

	return "logout", nil

}
