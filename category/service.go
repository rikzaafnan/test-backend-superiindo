package category

type Service interface {
	Create(input CategoryInput) (Category, error)
	FindAll() ([]Category, error)
	FindByName(name string) (Category, error)
}

type service struct {
	repository Repository
}

func NewService(repository Repository) *service {
	return &service{repository}
}

func (s *service) Create(input CategoryInput) (Category, error) {

	category := Category{}
	category.Name = input.Name

	newCategory, err := s.repository.Save(category)
	if err != nil {
		return newCategory, err
	}

	return newCategory, nil
}

func (s *service) FindAll() ([]Category, error) {

	categories, err := s.repository.FindAll()
	if err != nil {
		return categories, err
	}

	return categories, nil

}

func (s *service) FindByName(name string) (Category, error) {

	category, err := s.repository.FindByName(name)
	if err != nil {
		return category, err
	}

	return category, nil

}
