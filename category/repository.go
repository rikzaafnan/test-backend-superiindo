package category

import (
	"gorm.io/gorm"
)

type Repository interface {
	Save(in Category) (Category, error)
	FindAll() ([]Category, error)
	FindByName(name string) (Category, error)
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (r *repository) Save(in Category) (Category, error) {

	err := r.db.Create(&in).Error

	if err != nil {
		return in, err
	}

	return in, nil

}

func (r *repository) FindAll() ([]Category, error) {
	var categories []Category

	err := r.db.Find(&categories).Error
	if err != nil {

		return categories, err
	}

	return categories, nil
}

func (r *repository) FindByName(name string) (Category, error) {
	var categorie Category

	err := r.db.Where("name =?", name).First(&categorie).Error
	if err != nil {

		return categorie, err
	}

	return categorie, nil
}
